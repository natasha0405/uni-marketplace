/**

 *

 *      +----------------------------------------------------------------------------------------------------------------+

 *      |                                             Copyright - Northest Missouri State University                  | 

*       |-----------------------------------------------------------------------------------------------------------------|

 *      |Team 4                                                                                                                           | 

 *      |Unit 9: Varshitha Guntakandla , Phanivardhan Gurram                                                                                         | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |Description: This program  is to test the user models                                                                   | 

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |    NAME                            VERSION                       CHANGES                                    |

 *      |-----------------------------------------------------------------------------------------------------------------|

 *      |     Phanivardhan Gurram                                     0.0.1                     added user.js file                       | 

 *      |                                                                                                                                       | 

 *      |                                                                                                                                       | 

 *      +-----------------------------------------------------------------------------------------------------------------+

 */
  
   
  

 
process.env.NODE_ENV = 'test'
const app = require('../../app.js')
var User = require('../../models/user.js')
const LOG = require('../../utils/logger.js')
const mocha = require('mocha')
const expect = require('chai').expect
const find = require('lodash.find')
const EMAIL = 'test@test.com'
const PASSWORD = 'Password_1'

LOG.debug('Starting test/model/userTest.js.')

mocha.describe('API Tests - User Model', function () {
  var item
  mocha.before(function (done) {
    var testItem = new User({
      email: EMAIL,
      password: PASSWORD
    })
    app.locals.users.query.push(testItem)
    testItem.save(function (err) {
      if (err) {
        done(err)
      } else {
        const data = app.locals.users.query
        item = find(data, { email: EMAIL })
        done()
      }
    })
  })
  mocha.it('new item should not be null', function (done) {
    expect(item).to.not.equal(null)
    done()
  })
  mocha.it('it should have an _id', function (done) {
    expect(item._id).to.not.equal(null)
    LOG.debug('item._id = ' + item._id)
    done()
  })
  mocha.it('it should have the given email', function (done) {
    expect(item.email).to.equal(EMAIL)
    LOG.debug('User.email = ' + item.email)
    done()
  })
  mocha.it('it should have a password hash', function (done) {
    expect(item.password).to.not.equal(null)
    expect(item.password).to.not.equal(PASSWORD)
    LOG.debug('User.password = ' + item.password)
    done()
  })
})

mocha.describe('API Tests - User Model Invalid', function () {
  mocha.it('it should be invalid if email is empty', function (done) {
    var testItem = new User({
      password: PASSWORD
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
  mocha.it('it should be invalid if password is empty', function (done) {
    var testItem = new User({
      email: EMAIL
    })
    testItem.validate(function (err) {
      expect(err).not.equal(null)
      done()
    })
  })
})