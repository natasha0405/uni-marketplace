/** 
* Team 6  
* Unit 7 - Rohith Kumar Agiru , Kishan Kalburgi Srinivas
* set up a (in memory) database
*/
const Datastore = require('nedb')
const LOG = require('../utils/logger.js')
const estimates = require('../data/estimates.json')
const users = require('../data/users.json')

module.exports = (app) => {
 LOG.info('START seeder.')
 const db = new Datastore()
 db.loadDatabase()

 // insert the sample data into our data store
 db.insert(estimates)
 db.insert(users)

 // initialize app.locals (these objects will be available to our controllers)
 app.locals.estimates = db.find(estimates)
 app.locals.users = db.find(users)

 LOG.debug(`${estimates.length} estimates`)
 LOG.info('END Seeder. Sample data read and verified.')
}